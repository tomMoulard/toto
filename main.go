package main

import (
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"ithub.com/tommoulard/toto/docs"
)

//go:generate swag init --parseDependency --parseInternal

type totoResponse struct {
	Message string `json:"message"`
	Count   int    `json:"count"`
}

// totoHandler godoc
// @Summary Say hello to toto
// @Description Say hello to toto or tata
// @Produce json
// @Param q query string false "toto or tata"
// @Success 200 {object} totoResponse
// @Router /toto [get]
func totoHandler(c *gin.Context) {
	q := c.Query("q")
	if q == "tata" {
		c.JSON(200, totoResponse{
			Message: "Hello tata",
			Count:   2,
		})

		return
	}

	c.JSON(200, totoResponse{
		Message: "Hello toto",
		Count:   1,
	})
}

func main() {

	docs.SwaggerInfo.Title = "Toto API"
	docs.SwaggerInfo.BasePath = "/"
	docs.SwaggerInfo.Version = "1.0"
	docs.SwaggerInfo.Description = "This is a sample server for toto."
	docs.SwaggerInfo.Schemes = []string{"http"}

	router := gin.New()
	router.GET("/toto", totoHandler)
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	router.Run(":8080")
}
