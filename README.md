# toto

POC using https://github.com/swaggo/swag
Declarative format: https://github.com/swaggo/swag?tab=readme-ov-file#general-api-info

## Generate swagger doc

```bash
go install github.com/swaggo/swag/cmd/swag@latest
go generate
```

## Run

```bash
go run main.go
```

## Access

http://localhost:8080/swagger/index.html
